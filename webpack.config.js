var path = require('path')
var webpack = require('webpack')
var SpritesmithPlugin = require('webpack-spritesmith');
var CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const SassPlugin = require('sass-webpack-plugin');
const HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');

const makeSprite = (url) => new SpritesmithPlugin({
        src: {
            cwd: path.resolve(__dirname, 'src/img/' + url),
            glob: '*.png'
        },
        target: {
            image: path.resolve(__dirname, 'web/img/' + url + '.png'),
            css: path.resolve(__dirname, 'src/scss/_sprites/_' + url + '.scss')
        },
        apiOptions: {
            cssImageRef: "/img/" + url + ".png"
        }
    });

module.exports = {
    entry: [
        './src/js/main.js',
        './src/scss/style.scss'
    ],
    output: {
        path: path.resolve(__dirname, './web/js'),
        filename: '[name].js',
        publicPath: '/js/'
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '../css/[name].css',
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader?-url'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    },
    plugins: [
        makeSprite('ico'),
        makeSprite('main-menu'),
        makeSprite('m-main-menu'),
        new CopyWebpackPlugin([
            {from: 'src/img', to: '../img'},
            {from: 'src/dataimg', to: '../dataimg'},
//            {from:'src/fonts',to:'../fonts'},
            {from:'src/favicon.ico',to:'../'},
                    // {from:'src/index.html',to:'../'}
        ]),
//        new SassPlugin({'./src/scss/style.scss': '../css/style.css'}, process.env.NODE_ENV),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: '../index.html' //relative to root of the application
        }),
    ],
    devServer: {
        historyApiFallback: true,
        noInfo: true,
        overlay: true,
        contentBase: './web/',
        port: 8090,
    },
    performance: {
        hints: false
    },
    devtool: '#eval-source-map'
};

if (process.env.NODE_ENV === 'production') {
    module.exports.mode = 'production';
    module.exports.devtool = '#source-map'
    module.exports.optimization = {
        minimize: true
    }
    module.exports.plugins = (module.exports.plugins || []).concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ])
}
