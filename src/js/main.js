// basic example to fullpage-pagination plugin
// https://github.com/yurayarosh/fullpage-pagination

// import 'fullpage-pagination/fullpage-js.css';
import { Fullpage, addTouchEvents } from 'fullpage-pagination'
        import { tns } from "../../node_modules/tiny-slider/src/tiny-slider";
addTouchEvents()

class OEBFullpage extends Fullpage {
    constructor(page, options) {
        super(page, options)
        this.prevButton = options.prevButton
        this.nextButton = options.nextButton
    }

    onExit(section, resolve) {
        setTimeout(() => {
            resolve()
        }, 50)
    }
    onEnter(section, resolve) {
        setTimeout(() => {
            section.classList.add("ready");
            resolve()
        }, 500)
    }
    onComplete(section, resolve) {
        setTimeout(() => {
            resolve()
        }, 1000)
    }
}

const page = document.querySelector('.js-fullpage')
const navigation = document.querySelector('.js-fullpage-nav')
const prevButton = document.querySelector('.js-prev')
const nextButton = document.querySelector('.js-next')

const options = {
    prevButton,
    nextButton,
    navigation,
    delay: 100,
    transition: 500,
    easing: 'ease-out',
    touchevents: true,
    customTransition: false,
    fadeIn: false,
    fadeInDuration: 1000,
    renderNavButton: i => {
        return i < 9 ? `0${i + 1}` : i + 1
    },
    loop: false,
    toggleClassesFirst: false
}


const fullpage = new OEBFullpage(page, options);

document.addEventListener('DOMContentLoaded', function () {
    if(window.matchMedia("only screen and (min-width: 1300px)").matches){
        if (window.matchMedia("only screen and (min-height: 865px)").matches) {
            location.hash = "";
            document.querySelector(".fullpage").classList.add("created");
            fullpage.init();
        } else {
            document.querySelector(".fullpage").classList.add("narrow");
            document.querySelectorAll(".fullpage__section").forEach(function(node) {
                node.classList.add("ready");
            });
        }
        
        let video = document.querySelector(".main-item-video");
        let source = document.createElement('source');

        source.setAttribute('src', "/img/scr-1/m-i-v.webm");

        video.oncanplay = function() {
            if(document.getElementById("loader")){
                document.getElementById("loader").remove();
            }
            setTimeout( () => video.play(), 2000);
        };
        video.appendChild(source);
        video.removeAttribute( 'controls' );
    } else {
        document.getElementById("loader").remove();
    }
    document.getElementsByClassName("section-main")[0].classList.add("ready");

    document.querySelectorAll(".js-menu-toggle").forEach(function(node) {
        node.addEventListener("click", function () {
            document.querySelector(".mobile-menu").classList.toggle("active");
        });
    });

    const testimonialsSlider = tns({
        container: '.testimonials-slider',
        items: 1,
        slideBy: 'page',
        autoplay: false,
        controls: true,
        controlsText: ["", ""],
        responsive: {
            "500": {
                "items": 2
            },
            "1400": {
                "items": 6
            }
        },
    });
    const PricingSlider = tns({
        container: '.pricing-slider',
        items: 1,
        slideBy: 'page',
        autoplay: false,
        controls: true,
        controlsText: ["", ""],
        responsive: {
            "500": {
                "items": 2
            },
            "1400": {
                "items": 5
            }
        },
    });

}, false);
